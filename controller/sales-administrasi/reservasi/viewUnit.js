jQuery.post(targetUrl, dataFeed, function(data) {
	jQuery.each(data,function(i,value){
		// defining kontrol proses
		dataTemp.proses[value.idtabel_unit] = {proses: "rinciUnit", kode_unit: value.kode_unit, idtabel_unit: value.idtabel_unit};
		inHTML	= '<tr onclick="loadFile(\'' + value.idtabel_unit + '\')" style=\"cursor: pointer\">' +
				'<td>' + value.kode_unit + '</td>' +
				'<td align="right">' + jQuery.formatNumber(value.harga_unit, {format:'#,###', locale:'en'}) + '</td>' +
				'<td align="right">' + value.unit_book + '</td>' +
				'<td>' + value.status_unit + '</td>' +
			'</tr>';
		jQuery('#tabel-unit tbody').append(inHTML);
	});

	// storing kontrol proses
	localStorage.setItem("rasamala", JSON.stringify(dataTemp));

	// rendering tabel
	jQuery(document).ready(function() {
		jQuery('#tabel-unit').dataTable( {
			stateSave: true
		});
	});
}, 'json');
