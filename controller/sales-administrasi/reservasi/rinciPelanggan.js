jQuery('.block-title h2').html('Informasi Pelanggan');
jQuery('.block-title').removeClass('hidden');
jQuery('.rincian').removeClass('hidden');

jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		dataFeed	= {filter: [{name: 'idtabel_pelanggan', value: value.idtabel_pelanggan}]};
		inHTML 		= '<div class="form-group">' +
				'<label class="col-sm-2 control-label" for="input-normal"></label>' +
				'<div class="btn-group col-sm-5">' +
					'<button class="btn btn-sm btn-info" onclick="loadFile(\'kembali\')">' +
					'<i class="fa fa-arrow-circle-left"></i> Kembali' +
					'</button>' +
					'<button id="button-set" class="btn btn-sm btn-info"><i class="fa fa-pencil-square-o"></i> Set Aktif</button>' +
					'<button id="button-hapus" class="btn btn-sm btn-warning"><i class="fa fa-trash-o"></i> Hapus</button>' +
				'</div>' +
			'</div>';
		jQuery('form').prepend(inHTML);

		jQuery('textarea[name=alamat_pelanggan]').html(value.alamat_pelanggan);
		jQuery('input[name=nama_pelanggan]').val(value.nama_pelanggan);
		jQuery('input[name=kota_domisili]').val(value.kota_domisili);
		jQuery('input[name=kode_pos_domisili]').val(value.kode_pos_domisili);
		switch(value.index_kelamin){
			case '1':
				jQuery('#index_jantan').prop('checked', true);
				break;
			default:
				jQuery('#index_betina').prop('checked', true);
		}

		jQuery('#button-set').click(function(){
			jQuery('button').prop('disabled', true);
			targetUrl	= "/rasamala/api/pelanggan/session_pelanggan.php";
			jQuery.post(targetUrl, dataFeed, function(data){
				jQuery('#pesan').removeClass('hidden');
				jQuery('#pesan div[role=alert]').addClass(data.kelas);
				jQuery('#pesan div[role=alert]').html(data.pesan);
				jQuery('form').remove();
			}, "json");
			jQuery('button').prop('disabled', false);
		});

		jQuery('#button-hapus').click(function(){
			jQuery('button').prop('disabled', true);
			targetUrl	= "/rasamala/api/pelanggan/hapus_pelanggan.php";
			jQuery.post(targetUrl, dataFeed, function(data){
				jQuery('#pesan').removeClass('hidden');
				jQuery('#pesan div[role=alert]').addClass(data.kelas);
				jQuery('#pesan div[role=alert]').html(data.pesan);
				jQuery('form').remove();
			}, "json");
			jQuery('button').prop('disabled', false);
		});

	});
}, "json");

// define parameter kembali
dataTemp.proses.kembali	= {proses: dataTemp.applKode};
// storing kontrol proses
localStorage.setItem("rasamala", JSON.stringify(dataTemp));

jQuery('#button-sunting').click(function(){
	jQuery('input').prop('disabled', false);
	jQuery('textarea').prop('disabled', false);
	jQuery('.rincian').addClass('hidden');
	jQuery('.sunting').removeClass('hidden');
});

jQuery('#button-batal').click(function(){
	jQuery('.sunting').addClass('hidden');
	jQuery('.rincian').removeClass('hidden');
});
