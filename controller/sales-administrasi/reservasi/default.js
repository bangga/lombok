jQuery('input').prop('disabled', false);
jQuery('textarea').prop('disabled', false);
jQuery('.tambah').removeClass('hidden');
jQuery('.block-title h2').html('Form Reservasi');
jQuery('.block-title').removeClass('hidden');

// define parameter kembali
dataTemp.proses.kembali	= {proses: dataTemp.applKode};
// storing kontrol proses
localStorage.setItem("rasamala", JSON.stringify(dataTemp));

jQuery(function(){
	jQuery('#dpYears').datepicker().on('changeDate', function(ev){
		jQuery('#dpDate').text(jQuery('#dpYears').data('date'));
		jQuery('input[name=tanggal_kelahiran]').val(jQuery('#dpYears').data('date'));
	});
});

jQuery('#button-tambah').click(function(){
	jQuery('button').prop('disabled', true);
	dataFeed = {data: jQuery('.form-pelanggan').serializeArray()};
	targetUrl	= "/rasamala/api/pelanggan/insert_pelanggan.php";
	jQuery.post(targetUrl, dataFeed, function(data){
		jQuery('#pesan').removeClass('hidden');
		jQuery('#pesan div[role=alert]').addClass(data.kelas);
		jQuery('#pesan div[role=alert]').html(data.pesan);
		jQuery('form').remove();
	}, "json");
	jQuery('button').prop('disabled', false);
});
