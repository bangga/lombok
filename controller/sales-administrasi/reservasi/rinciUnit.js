jQuery('input[name=kode_unit]').prop('placeholder',dataFeed.kode_unit);
jQuery.post(targetUrl, dataFeed, function(data){
	inHTML = '';
	if(data.length==0){
		inHTML	= '<button class="btn btn-sm btn-info">Belum dipilih <span class="badge">0</span></button>';
	}

	jQuery.each(data,function(i,value){
		inHTML = inHTML + '<button class="btn btn-sm btn-info">' + value.kode_unit + ' <span class="badge">' + value.unit_book + '</span></button>';
		if(dataFeed.idtabel_unit==value.idtabel_unit){
			jQuery('input[name=jumlah_peminat]').prop('placeholder',value.unit_book);
		}
	});

	jQuery('#unit-book').html(inHTML);

	// storing kontrol proses
	dataTemp.proses.kembali	= {proses: dataTemp.applKode};
	localStorage.setItem("rasamala", JSON.stringify(dataTemp));

	dataFeed.data = [{name: 'idtabel_unit', value: dataFeed.idtabel_unit}];
	delete dataFeed.proses;
	delete dataFeed.idtabel_unit;
	delete dataFeed.kode_unit;
	jQuery('#button-simpan').click(function(){
		jQuery('button').prop('disabled', true);
		if(dataTemp.applKode=='020501'){
			targetUrl	= "/rasamala/api/unit/reservasi/insert_unit.php";
		}
		else{
			targetUrl	= "/rasamala/api/unit/reservasi/insert_book.php";
		}
		jQuery.post(targetUrl, dataFeed, function(data){
			jQuery('#pesan').removeClass('hidden');
			jQuery('#pesan div[role=alert]').addClass(data.kelas);
			jQuery('#pesan div[role=alert]').html(data.pesan);
			jQuery('form').remove();
		}, "json");
		jQuery('button').prop('disabled', false);
	});

}, 'json');
