jQuery.post(targetUrl, dataFeed, function(data) {
	jQuery.each(data,function(i,value){
		// defining kontrol proses
		dataTemp.proses[value.idtabel_pelanggan] = {proses: "rinciPelanggan", filter: [{name: "idtabel_pelanggan", value: value.idtabel_pelanggan}]};
		inHTML	= '<tr onclick="loadFile(\'' + value.idtabel_pelanggan + '\')" style=\"cursor: pointer\">' +
				'<td>' + value.kode_pelanggan + '</td>' +
				'<td>' + value.nama_pelanggan + '</td>' +
				'<td>' + value.alamat_pelanggan + '</td>' +
				'<td>' + value.ketertarikan + '</td>' +
			'</tr>';
		jQuery('#tabel-pelanggan tbody').append(inHTML);
	});

	// storing kontrol proses
	localStorage.setItem("rasamala", JSON.stringify(dataTemp));

	// rendering tabel
	jQuery(document).ready(function() {
		jQuery('#tabel-pelanggan').dataTable( {
			stateSave: true
		});
	});

}, 'json');
