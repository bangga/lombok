jQuery('.block-title h2').html(dataFeed.title);

if(dataFeed.proses=='tambahUser'){
	// remove grup button sunting
	jQuery('.rincian').remove();
	jQuery('.sunting').remove();
	// show grup button tambah
	jQuery('.tambah').removeClass('hidden');

	// inquiri grup selector
	getSelectGrup('');

	// inisialisasi proses tambah user
	jQuery('#button-simpan').click(function(){
		dataFeed 			= {data: jQuery(':input').serializeArray()};
		if(dataFeed.data[0].value.length>0 && dataFeed.data[1].value.length>0){
			jQuery('button').prop('disabled', true);
			targetUrl	= "./api/referensi/tambah_user.php";
			jQuery.post(targetUrl, dataFeed, function(data){
				jQuery('#pesan').removeClass('hidden');
				jQuery('#pesan div').addClass(data.kelas);
				jQuery('#pesan div').html(data.pesan);
				jQuery('form').remove();
				jQuery('button').prop('disabled', false);
			}, "json");
		}
	});
}
else{
	// remove grup button tambah
	jQuery('.tambah').remove();
	// show grup button rinci
	jQuery('.rincian').removeClass('hidden');
	// disable all input form
	jQuery('input').attr('disabled', true);
	jQuery('select').attr('disabled', true);
	// inquiri data user
	targetUrl   = "./api/referensi/view_user.php";
	jQuery.post(targetUrl, {filter: dataFeed.filter}, function(data){
		jQuery('input[name=usr_id]').attr('value', data[0].usr_id);
		jQuery('input[name=usr_nama]').attr('value', data[0].usr_nama);

		// inquiri grup selector
		getSelectGrup(data[0].grup_id);

		// show grup button sunting
		jQuery('#button-sunting').click(function(){
			jQuery('input').attr('disabled', false);
			jQuery('select').attr('disabled', false);
			jQuery('.sunting').removeClass('hidden');
			jQuery('.rincian').addClass('hidden');
		});

		// show grup button rincian
		jQuery('#button-batal').click(function(){
			jQuery('input').attr('disabled', true);
			jQuery('select').attr('disabled', true);
			jQuery('.sunting').addClass('hidden');
			jQuery('.rincian').removeClass('hidden');
		});

		// define proses hapus user
		jQuery('#button-hapus').click(function(){
			if(confirm('Yakin akan menghapus pengguna ' + data[0].usr_nama + ' ?')){
				dataFeed 	= {filter: dataFeed.filter};
				jQuery('button').prop('disabled', true);
				targetUrl	= "./api/referensi/hapus_user.php";
				jQuery.post(targetUrl, dataFeed, function(data){
					jQuery('#pesan').removeClass('hidden');
					jQuery('#pesan div').addClass(data.kelas);
					jQuery('#pesan div').html(data.pesan);
					jQuery('form').remove();
					jQuery('button').prop('disabled', false);
				}, "json");
			}
			else{
				jQuery('button').prop('disabled', false);
			}
		});

		// define proses simpan perubahan
		jQuery('#button-simpan').click(function(){
			dataFeed 	= {filter: dataFeed.filter, data: jQuery(':input').serializeArray()};
			delete dataFeed.data[0];
			delete dataFeed.data[1];
			delete dataFeed.data[2];
			delete dataFeed.data[3];
			jQuery('button').prop('disabled', true);
			targetUrl	= "./api/referensi/sunting_user.php";
			jQuery.post(targetUrl, dataFeed, function(data){
				jQuery('#pesan').removeClass('hidden');
				jQuery('#pesan div').addClass(data.kelas);
				jQuery('#pesan div').html(data.pesan);
				jQuery('form').remove();
				jQuery('button').prop('disabled', false);
			}, "json");
		});

	}, "json");
}

// set tombol kembali ke menu sebelumnya
jQuery('#button-kembali').click(function(){
	loadMenu(dataTemp.applKode);
});

// set tombol kembali ke menu sebelumnya
jQuery('#button-selesai').click(function(){
	loadMenu(dataTemp.applKode);
});
