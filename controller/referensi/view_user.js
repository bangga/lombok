// halaman data user : front
jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		// defining kontrol proses
		dataTemp.proses[value.usr_id]	= {proses: "rinciUser", title: value.usr_nama, filter: [{name: "usr_id", value: value.usr_id}]};

		jQuery('#tabel-user tbody').append('<tr onclick="loadFile(\'' + value.usr_id + '\')" style=\"cursor: pointer\"><td>' + value.usr_id + '</td><td>' + value.usr_nama + '</td><td>' + value.grup_nama + '</td><td>' + value.hak_akses + '</td></tr>');
	});

	// define parameter tambah user
	dataTemp.proses.tambah = {proses: 'tambahUser', title: 'Form Tambah User'};

	// storing kontrol proses
	localStorage.setItem("rasamala", JSON.stringify(dataTemp));

	// set event tambah user
	jQuery('#tambah-user').click(function(){
		loadFile('tambah');
	});

	// rendering tabel
	jQuery(document).ready(function(){
		jQuery('#tabel-user').dataTable({
			stateSave: true
		});
	});
}, "json");
