// halaman data grup : front
jQuery.post(targetUrl, {}, function(data){
	jQuery.each(data,function(i,value){
		// defining kontrol proses
		dataTemp.proses[value.grup_id] = {proses: 'menuAkses', title: value.grup_nama, filter: [{name: "grup_id", value: value.grup_id}]};
		
		jQuery('#tabel-grup tbody').append('<tr onclick="loadFile(\'' + value.grup_id + '\')" style=\"cursor: pointer\"><td>' + value.grup_nama+ '</td></tr>');
	});

	// storing kontrol proses
	localStorage.setItem("rasamala", JSON.stringify(dataTemp));

	// rendering table
	jQuery('#tabel-grup').dataTable({
		stateSave: true
	});
}, "json");
