// halaman data akses : front
jQuery('.block-title h2').html(dataFeed.title);
jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		jQuery('#tabel-akses tbody').append('<tr><td>' + value.parent_nama + '</td><td>' + value.appl_nama + '</td><td>' + value.appl_deskripsi + '</td></tr>');
	});

	// set tombol kembali ke menu sebelumnya
	jQuery('#button-kembali').click(function(){
		loadMenu(dataTemp.applKode);
	});

	// rendering tabel
	jQuery(document).ready(function(){
		jQuery('#tabel-akses').dataTable({
			stateSave: true
		});
	});
}, "json");

