jQuery('#main-container').css('min-height', jQuery(window).height());
jQuery('#page-content').css('min-height', jQuery(window).height() - 84);

jQuery.ajaxSetup ({
    cache: false
});

function toggleMenu(param){
	jQuery('.sidebar-nav-menu').removeClass('open');
	jQuery('.open-' + param).addClass('open');
	jQuery('.sidebar-nav li ul').fadeIn().css('display', 'none');
	jQuery('.' + param).css('display', 'block');
}

function toggleSide(param){
	jQuery('li').removeClass('active');
	jQuery('#' + param).addClass('active');
	jQuery('.sidebar-nav').addClass('hidden');
	jQuery('.' + param).removeClass('hidden');
}

function getToken(){
    randId = new Date();
    return randId.getTime();
}

function getSelectGrup(grupId){
	targetUrl   = "./api/referensi/view_grup.php";
	jQuery.post(targetUrl, {}, function(data){
		jQuery('#option-group').append('<option value="500"> - </option>');
		jQuery.each(data,function(i,value){
			inAttr	= "";
			if(grupId==value.grup_id){
				inAttr	= "selected";
			}
			jQuery('#option-group').append('<option value="' + value.grup_id + '" ' + inAttr + '>' + value.grup_nama + '</option>');
		});
	}, "json");
}

// load menu utama
var applName	= "";
var targetDir	= "";
function loadMenu(param){
	jQuery('li').removeClass('active');
	jQuery('#' + param).addClass('active');
	dataTemp	= JSON.parse(localStorage.rasamala);
	dataFeed	= dataTemp[param];
	applName	= dataFeed.applName;
	targetDir	= dataFeed.targetUrl
	inHTML		= '<i class="fa fa-html5"></i>' + dataFeed.applName + '<br><small>' + dataFeed.applDesc + '</small>';

	// breadcrumb
	if(!jQuery('.breadcrumb').hasClass('hide')){
		jQuery('.breadcrumb').addClass('hide');
	}

	// set menu aktif
	targetUrl			= dataFeed.targetUrl + 'index.html';
	dataTemp.applKode	= param;
	delete dataTemp.procKode;
	delete dataTemp.proses;
	localStorage.setItem("rasamala", JSON.stringify(dataTemp));

	jQuery('#page-content div div h1').html(inHTML);
	jQuery('.block').load(targetUrl);
}

// nama bulan
function namaBulan(param){
	switch(parseInt(param)){
		case 1:
			return 'JANUARI';
			break;
		case 2:
			return 'FEBRUARI';
			break;
		case 3:
			return 'MARET';
			break;
		case 4:
			return 'APRIL';
			break;
		case 5:
			return 'MEI';
			break;
		case 6:
			return 'JUNI';
			break;
		case 7:
			return 'JULI';
			break;
		case 8:
			return 'AGUSTUS';
			break;
		case 9:
			return 'SEPTEMBER';
			break;
		case 10:
			return 'OKTOBER';
			break;
		case 11:
			return 'NOPEMBER';
			break;
		case 12:
			return 'DESEMBER';
			break;
		default:
			return 'PILIH BULAN';
	}
}

// load proses in-menu
function loadFile(param){
	dataTemp	= JSON.parse(localStorage.rasamala);
	dataFeed	= {proses: dataTemp.proses[param].proses};
	targetUrl	= dataTemp[dataTemp.applKode].targetUrl + 'index.html';

	// set proses aktif
	dataTemp.procKode	= param;
	localStorage.setItem("rasamala", JSON.stringify(dataTemp));

	jQuery('.block').load(targetUrl);
}

// load modal form
var dataTemp	= {};
var dataFeed	= {};
var targetUrl   = "";
function loadForm(param){
	dataTemp			= JSON.parse(localStorage.rasamala);
	dataTemp.proses		= {};
	dataFeed.targetId	= getToken();
	jQuery(document.getElementsByClassName(param)).each(function(i,value){
		dataFeed[value.name] = value.value;
	});

	if(dataFeed.proses=='gantiPassword'){
		targetUrl	= dataFeed.targetUrl;
	}
	else if(typeof(dataTemp.applKode)=='string'){
		targetUrl	= dataTemp[dataTemp.applKode].targetUrl + dataFeed.targetUrl;
	}
	else{
		targetUrl	= dataFeed.targetUrl;
	}
	jQuery('body').prepend('<div id="' + dataFeed.targetId + '" class="modal fade"></div>');
	jQuery('#' + dataFeed.targetId).load(targetUrl);
	delete dataFeed.targetUrl;
	dataTemp.proses[dataFeed.proses] = dataFeed;

	// simpan ke local storage
	localStorage.setItem("rasamala", JSON.stringify(dataTemp));
}

function loadProc(param){
	targetUrl	= param.targetUrl;
	delete param.targetUrl;
	jQuery.post(targetUrl, param, function(data){
		jQuery('form').html(data);
	});
}

// generate menu aplikasi
var kunci		= "";
var randId		= "";
var inHTML		= "";
var inAttr		= "";
var dataProc	= {};

targetUrl   	= "./api/get_menu.php";
jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data, function(key, value){
		kunci = key;
		jQuery.each(value, function(key, value){
			if(kunci=='000000'){
				if(value.appl_kode.substr(4,2)=='00'){
					inHTML	= '<ul class="sidebar-nav ' + value.appl_kode + ' hidden"></ul>'
					jQuery('.sidebar-content').append(inHTML);
					inHTML	= '<li id="' + value.appl_kode + '">' +
								'<a onclick="toggleSide(\'' + value.appl_kode + '\')" style="cursor: pointer">' + value.appl_icon + '</a>' +
							'</li>';
				}
				else{
					if(value.appl_kode=='050101'){
						inHTML	= '<li id="' + value.appl_kode + '">' +
									'<a onclick="window.open(\'controller/display/\',\'display\',\'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no,fullscreen=yes\')" style="cursor: pointer">' + value.appl_name + '</a>' +
								'</li>';
					}
					else{
						dataTemp[value.appl_kode]	= {targetUrl: value.appl_file, applKode: value.appl_kode, applName: value.appl_name, applDesc: value.appl_deskripsi};
						inHTML	= '<li id="' + value.appl_kode + '">' +
									'<a onclick="loadMenu(\'' + value.appl_kode + '\')" style="cursor: pointer">' + value.appl_name + '</a>' +
								'</li>';
					}
				}
				jQuery('.navbar-nav').append(inHTML);
			}
			else {
				if(value.appl_kode.substr(4,2)=='00'){
					inHTML = '<li onclick="toggleMenu(\'' + value.appl_kode + '\')"><a class="sidebar-nav-menu open-' + value.appl_kode + '" style="cursor: pointer"><i class="fa fa-angle-left sidebar-nav-indicator"></i><i class="fa fa-th-large sidebar-nav-icon"></i>' + value.appl_name + '</a><ul class="' + value.appl_kode + '" style="display: none"></ul></li>';
				}
				else{
					dataTemp[value.appl_kode]	= {targetUrl: value.appl_file, applKode: value.appl_kode, applName: value.appl_name, applDesc: value.appl_deskripsi};
					inHTML = '<li><a onclick="loadMenu(\'' + value.appl_kode + '\')" style="cursor: pointer">' + value.appl_name + '</a></li>';
				}
				jQuery('.' + kunci).append(inHTML);
			}
		});
	});
	localStorage.setItem("rasamala", JSON.stringify(dataTemp));
}, "json");
