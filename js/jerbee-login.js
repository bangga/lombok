jQuery.ajaxSetup ({
	xhrFields: {
	  withCredentials: true
	},
    cache: false
});

jQuery('#button-login').click(function(){
	var dataFeed 	= {data: jQuery(':input').serializeArray()};
	var targetUrl	= 'api/referensi/login_user.php';
	jQuery.post(targetUrl, dataFeed, function(data){
		if(data.errno==0){
			document.location.href = './';
		}
		else{
			alert(data.pesan);
			console.log(data);
		}
	}, 'json');
});
