<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');

	include $_SERVER['DOCUMENT_ROOT']."/api/setDB01.php";

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$nilai	= $_POST['filter'];
	$query	= "DELETE FROM tr_user WHERE usr_id='".$nilai[0]['value']."'";
	/* getParam **/

	$errno	= 0;
	if(strlen($nilai[0]['value'])>=3){
		try{
			$PLINK->beginTransaction();
			if($PLINK->exec($query)>0){
				$pesan 	= "Data telah berhasil dihapus";
				$kelas	= "alert alert-success";
			}
			else{
				$pesan 	= "Data tidak bisa dihapus";
				$kelas	= "alert alert-info";
				$errno	= 2;
			}
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$pesan	= "Data gagal dihapus";
			$kelas	= "alert alert-warning";
			$errno	= 1;
		}
	}
	else{
		$pesan	= "Permintaan tidak dapat diterima";
		$kelas	= "alert alert-warning";
		$errno	= 1;
	}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "errno"=>$errno);
	echo json_encode($pesan);
?>
