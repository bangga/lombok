<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');

	session_start();
	if(isset($_SESSION['User_c'])){
		$usr_id		= $_SESSION['User_c'];
		$grup_id	= "";
		include $_SERVER['DOCUMENT_ROOT']."/api/setDB01.php";

		/** getParam 
			memindahkan semua nilai dalam array POST ke dalam
			variabel yang bersesuaian dengan masih kunci array
		*/
		if(isset($_POST['filter'])){
			$filter	= array();
			$nilai	= $_POST['filter'];
			for($i=0;$i<count($nilai);$i++){
				$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
			}
			if($i>0){
				$filter	= "WHERE ".implode(' AND ',$filter);
			}
		}
		else{
			$filter	= "";
		}
		/* getParam **/

		/* database **/
		try {
			$que 	= "SELECT * FROM view_user ".$filter;
			$sth 	= $PLINK->prepare($que);
			$sth->execute();
			$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
			$PLINK 	= null;
		}
		catch (PDOException $e){
			$row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
		}
	}
	else{
		$row	= array("pesan"=>"Login telah expired", "errno"=>1);
	}

	echo json_encode($row);
    flush();
?>
