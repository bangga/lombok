<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');

	session_start();
	$usr_id	= $_SESSION['User_c'];
	session_destroy();

	define("_KODE", "000000");
	define("_USER", $usr_id);
	define("_HOST", $_SERVER['REMOTE_ADDR']);

	$pesan	= "Proses logout telah dilakukan";
	$error	= "";
	$errno	= 0;
	$row    = array("pesan"=>$pesan, "error"=>$error, "errno"=>$errno);

	echo json_encode($row);
    flush();
?>
