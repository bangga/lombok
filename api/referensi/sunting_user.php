<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');

	$datadir	= explode('/',$_SERVER['PHP_SELF']);
	include $_SERVER['DOCUMENT_ROOT']."/".$datadir[1]."/api/setDB01.php";

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$query	= "UPDATE tr_user SET ";
	$nilai	= $_POST['data'];
	for($i=0;$i<count($nilai);$i++){
		if(isset($nilai[$i]['name'])){
			$query	.= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
			if(($i+1)<count($nilai)){
				$query	.= ",";
			}
		}
	}
	$nilai	= $_POST['filter'];
	$query	.= " WHERE ".$nilai[0]['name']."='".$nilai[0]['value']."'";
	/* getParam **/

	if(strlen($nilai[0]['value'])>=3){
		try{
			$PLINK->beginTransaction();
			if($PLINK->exec($query)>0){
				$pesan 	= "Data telah berhasil disimpan";
				$kelas	= "alert alert-success";
			}
			else{
				$pesan 	= "Data tidak bisa disimpan";
				$kelas	= "alert alert-info";
			}
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$pesan	= "Data gagal disimpan";
			$kelas	= "alert alert-warning";
		}
	}
	else{
		$pesan	= "Permintaan tidak dapat diproses";
		$kelas	= "alert alert-warning";
	}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "query"=>$query);
	echo json_encode($pesan);
?>
