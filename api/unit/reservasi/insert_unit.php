<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	session_start();

	include $_SERVER['DOCUMENT_ROOT']."/rasamala/api/setDB01.php";
	function getToken(){
		$acak	= mt_rand(1,9999);
		return date('ymdHis').str_repeat('0',4-strlen($acak)).$acak;
	}
	$idtabel_reservasi	= getToken();

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$nilai	= $_POST['data'];
	for($i=0;$i<count($nilai);$i++){
		$$nilai[$i]['name']	= $nilai[$i]['value'];
	}
	/* getParam **/

	$error	= "";
	$errno	= 0;
	if(isset($_SESSION['User_c'])){
		$usr_id				= $_SESSION['User_c'];
		$idtabel_pelanggan	= $_SESSION['Cust_c'];
		$que				= "SELECT COUNT(*) AS reff FROM tabel_reservasi WHERE idtabel_pelanggan=".$idtabel_pelanggan;
		try{
			$PLINK->beginTransaction();
			$que	= "INSERT INTO tabel_reservasi(idtabel_reservasi,idtabel_pelanggan,idtabel_unit,id_user,kode_reservasi,tanggal_reservasi) VALUES('".$idtabel_reservasi."','".$idtabel_pelanggan."','".$idtabel_unit."','".$usr_id."',1,NOW())";
			if($PLINK->exec($que)>0){
				$pesan 	= "Data telah berhasil disimpan";
				$kelas	= "alert alert-success";
			}
			else{
				$pesan 	= "Data tidak bisa disimpan";
				$kelas	= "alert alert-info";
			}
			$PLINK->commit();
			$_SESSION['Cost_c'] = $idtabel_pelanggan;
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$pesan	= "Data gagal disimpan";
			$kelas	= "alert alert-warning";
			$error	= $e->getMessage();
			$errno	= $e->getCode();
			if($errno==23000){
				$pesan = "Unit telah masuk ke daftar pemesanan";
			}
		}
	}
	else{
		$pesan	= "Permintaan tidak dapat diterima";
		$kelas	= "alert alert-warning";
	}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "errno"=>$errno, "query"=>$que);
	echo json_encode($pesan);
?>
