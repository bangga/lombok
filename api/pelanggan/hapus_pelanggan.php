<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	session_start();
	include $_SERVER['DOCUMENT_ROOT']."/rasamala/api/setDB01.php";

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_SESSION['User_c'])){
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$$nilai[$i]['name']	= $nilai[$i]['value'];
		}
	}
	/* getParam **/

	/* database **/
	try {
		$PLINK->beginTransaction();
		$que 	= "DELETE FROM tabel_pelanggan WHERE idtabel_pelanggan=".$idtabel_pelanggan;
		if($PLINK->exec($que)>0){
			$pesan 	= "Data telah berhasil dihapus";
			$kelas	= "alert alert-success";
		}
		else{
			$pesan 	= "Data tidak bisa dihapus";
			$kelas	= "alert alert-info";
		}
		$PLINK->commit();
	}
	catch (PDOException $e){
		$PLINK->rollBack();
		$pesan	= "Data gagal dihapus";
		$kelas	= "alert alert-warning";
		$error	= $e->getMessage();
	}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$que);
	echo json_encode($pesan);
	flush();
?>
