<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	session_start();

	include $_SERVER['DOCUMENT_ROOT']."/rasamala/api/setDB01.php";
	function getToken(){
		$acak	= mt_rand(1,9999);
		return date('z').str_repeat('0',4-strlen($acak)).$acak;
	}
	$idtabel_pelanggan	= getToken();

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$nilai	= $_POST['data'];
	for($i=0;$i<count($nilai);$i++){
		$$nilai[$i]['name']	= $nilai[$i]['value'];
	}
	/* getParam **/

	$error		= "";
	if(isset($_SESSION['User_c'])){
		$usr_id	= $_SESSION['User_c'];
		if(strlen($tanggal_kelahiran)<10){
			$tanggal_kelahiran = '01-01-1980';
		}
		try{
			$PLINK->beginTransaction();
			$que	= "INSERT INTO tabel_pelanggan(idtabel_pelanggan,nama_pelanggan,alamat_pelanggan,kota_domisili,kode_pos_domisili,telepon_domisili,fax_domisili,telepon_seluler,kode_negara_seluler,email,kota_kelahiran,tanggal_kelahiran,jenis_kelamin,nama_perusahaan,kontak_perusahaan,posisi_kontak,kotak_surat,status_kepemilikan,remark,ketertarikan,id_identititas,usr_id) VALUES('".$idtabel_pelanggan."','".$nama_pelanggan."','".$alamat_pelanggan."','".$kota_domisili."','".$kode_pos_domisili."','".$telepon_domisili."','".$fax_domisili."','".$telepon_seluler."','".$kode_negara_seluler."','".$email."','".$kota_kelahiran."',STR_TO_DATE('".$tanggal_kelahiran."','%d-%m-%Y'),'".$jenis_kelamin."','".$nama_perusahaan."','".$kontak_perusahaan."','".$posisi_kontak."','".$kotak_surat."','".$status_kepemilikan."','".$remark."','".$ketertarikan."','".$id_identititas."','".$usr_id."')";
			if($PLINK->exec($que)>0){
				$pesan 	= "Data telah berhasil disimpan";
				$kelas	= "alert alert-success";
			}
			else{
				$pesan 	= "Data tidak bisa disimpan";
				$kelas	= "alert alert-info";
			}
			$PLINK->commit();
			$_SESSION['Cust_c'] = $idtabel_pelanggan;
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$pesan	= "Data gagal disimpan";
			$kelas	= "alert alert-warning";
			$error	= $e->getMessage();
		}
	}
	else{
		$pesan	= "Permintaan tidak dapat diterima";
		$kelas	= "alert alert-warning";
	}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$que);
	echo json_encode($pesan);
?>
