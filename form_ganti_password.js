jQuery('#' + dataFeed.targetId).modal('show');
jQuery('.modal-title').html(dataFeed.title);

// set tombol tutup modal
jQuery('#button-kembali').click(function(){
	jQuery('#' + dataFeed.targetId).modal('hide');
	jQuery('#' + dataFeed.targetId).remove();
});

// proses sunting password
jQuery('#button-simpan').click(function(){
	jQuery('#button-simpan').prop('disabled', true);
	dataProc	= jQuery('.simpan').serializeArray();
	if(dataProc[1].value==dataProc[2].value){
		targetUrl	= "./api/referensi/sunting_password.php";
		jQuery.post(targetUrl, {data: dataProc}, function(data){
			jQuery('#pesan').removeClass('hidden');
			jQuery('#pesan div').addClass(data.kelas);
			jQuery('#pesan div').html(data.pesan);
			jQuery('form').remove();
			jQuery('#button-simpan').remove();
		}, "json");
	}
	else{
		jQuery('#pesan').removeClass('hidden');
		jQuery('#pesan div').addClass('alert alert-info');
		jQuery('#pesan div').html('Password tidak dapat diverifikasi');
		jQuery('#button-simpan').prop('disabled', false);
	}
});
